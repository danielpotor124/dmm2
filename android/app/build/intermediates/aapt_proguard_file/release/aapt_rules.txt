-keep class androidx.core.app.CoreComponentFactory { <init>(); }
-keep class androidx.room.MultiInstanceInvalidationService { <init>(); }
-keep class androidx.work.impl.WorkManagerInitializer { <init>(); }
-keep class androidx.work.impl.background.systemalarm.ConstraintProxy$BatteryChargingProxy { <init>(); }
-keep class androidx.work.impl.background.systemalarm.ConstraintProxy$BatteryNotLowProxy { <init>(); }
-keep class androidx.work.impl.background.systemalarm.ConstraintProxy$NetworkStateProxy { <init>(); }
-keep class androidx.work.impl.background.systemalarm.ConstraintProxy$StorageNotLowProxy { <init>(); }
-keep class androidx.work.impl.background.systemalarm.ConstraintProxyUpdateReceiver { <init>(); }
-keep class androidx.work.impl.background.systemalarm.RescheduleReceiver { <init>(); }
-keep class androidx.work.impl.background.systemalarm.SystemAlarmService { <init>(); }
-keep class androidx.work.impl.background.systemjob.SystemJobService { <init>(); }
-keep class androidx.work.impl.diagnostics.DiagnosticsReceiver { <init>(); }
-keep class androidx.work.impl.foreground.SystemForegroundService { <init>(); }
-keep class androidx.work.impl.utils.ForceStopRunnable$BroadcastReceiver { <init>(); }
-keep class app.notifee.core.BlockStateBroadcastReceiver { <init>(); }
-keep class app.notifee.core.ForegroundService { <init>(); }
-keep class app.notifee.core.ReceiverService { <init>(); }
-keep class com.dmm.MainActivity { <init>(); }
-keep class com.dmm.MainApplication { <init>(); }
-keep class com.google.android.datatransport.runtime.backends.TransportBackendDiscovery { <init>(); }
-keep class com.google.android.datatransport.runtime.scheduling.jobscheduling.AlarmManagerSchedulerBroadcastReceiver { <init>(); }
-keep class com.google.android.datatransport.runtime.scheduling.jobscheduling.JobInfoSchedulerService { <init>(); }
-keep class com.google.android.gms.auth.api.signin.RevocationBoundService { <init>(); }
-keep class com.google.android.gms.auth.api.signin.internal.SignInHubActivity { <init>(); }
-keep class com.google.android.gms.common.api.GoogleApiActivity { <init>(); }
-keep class com.google.firebase.components.ComponentDiscoveryService { <init>(); }
-keep class com.google.firebase.iid.FirebaseInstanceIdReceiver { <init>(); }
-keep class com.google.firebase.messaging.FirebaseMessagingService { <init>(); }
-keep class com.google.firebase.provider.FirebaseInitProvider { <init>(); }
-keep class com.reactnativecommunity.webview.RNCWebViewFileProvider { <init>(); }
-keep class io.invertase.firebase.messaging.ReactNativeFirebaseMessagingHeadlessService { <init>(); }
-keep class io.invertase.firebase.messaging.ReactNativeFirebaseMessagingReceiver { <init>(); }
-keep class io.invertase.firebase.messaging.ReactNativeFirebaseMessagingService { <init>(); }
-keep class io.invertase.notifee.NotifeeInitProvider { <init>(); }
-keep class android.widget.Space { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.app.AlertController$RecycleListView { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.view.menu.ActionMenuItemView { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.view.menu.ExpandedMenuView { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.view.menu.ListMenuItemView { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.ActionBarContainer { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.ActionBarContextView { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.ActionBarOverlayLayout { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.ActionMenuView { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.ActivityChooserView$InnerLayout { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.AlertDialogLayout { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.ButtonBarLayout { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.ContentFrameLayout { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.DialogTitle { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.FitWindowsFrameLayout { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.FitWindowsLinearLayout { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.SearchView$SearchAutoComplete { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.Toolbar { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.appcompat.widget.ViewStubCompat { <init>(android.content.Context, android.util.AttributeSet); }

-keep class androidx.core.widget.NestedScrollView { <init>(android.content.Context, android.util.AttributeSet); }

